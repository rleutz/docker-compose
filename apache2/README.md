# Lighttpd/Apache2 HTTP Server - Servidor Web

## Sobre:

Lighttpd - Apache Web Server, projeto licenciado sob a licença Apache.

#

## Observações:

- Este container não possui suporte a diversos módulos, como o PHP. Brevemente iremos disponibilizar uma solução para tanto;

- Provisoriamente, o usuário pode acessar o container via bash/shell para instalar tais módulos:

<pre>docker exec -ti apache bash
apt update
apt install [nome-do-módulo] </pre>

- A pasta para armazenamento de arquivos pode ser acessada no diretório /usr/local/apache2/htdocs.

#

## Comandos:

Após a configuração, a inicialização do container pode ser feita via:
<pre>(docker-compose -f caminho-docker-compose.yml up -d)</pre> 



Mais informações acerca da configuração do seu servidor Apache, acesse o site do projeto:
<href>https://httpd.apache.org/docs/</href>

#

- Autor:
  - Rodrigo B. de Sousa Martins - @rodsmar

#
* Este arquivo docker-compose.yml está licenciado sob a General Public License Version 2 (GPLv2), disponível na raiz deste repositório.
