# Pi-Hole - Servidor DNS/DHCP baseado no DNSMASQ
#### Versão do docker-compose: v2.3
#


Arquivo docker-compose do projeto Pi-Hole, licenciado sob a European Union Public License (EUPL) - https://joinup.ec.europa.eu/community/eupl/og_page/eupl

### Link do Projeto

<pre>https://github.com/pi-hole/pi-hole</pre>

### Observações

- A utilização deste container utilizando macvlan ou ipvlan é recomendada, podendo ser utilizado em modo host (não recomendado, em razão do servidor DHCP);
- Em modo host, é necessário redirecionar as portas 80 e 53 (caso seja utilizado apenas o servidor DNS);
- A utilização de endereços IPv6 não é mandatória, porém é recomendada caso o ambiente donde for instalado possui acesso à internet via IPv6;
- O IPv6 não precisa ter acesso à internet para que as máquinas clientes requisitem informações junto ao DNS (podendo ser utilizado endereços privados, ex: 2001:db8::/32);
- Os servidores DNS podem ser alterados no arquivo docker-compose.yml como também na interface web do Pi-Hole (menu settings, aba DNS), podendo inclusive serem definidos servidores diferentes dos que constam na lista "Upstream DNS Servers" (ex: os servidores DNS do seu provedor de internet);
- A opção "Listen on all interfaces, permit all origins", situada em Settings -> DNS, somente é recomendada quando o container for protegido por firewall, a fim de evitar ataques ao servidor DNS;

Após concluídas as modificações necessárias no docker-compose.yml, adaptando-o a sua rede (e/ou as suas necessidades) é possível iniciar utilizando o comando:

<pre>docker-compose -f (CAMINHO DO DOCKER-COMPOSE.YML) up -d</pre>

Arquivo docker-compose.yml licenciado via GPLv2, disposta no arquivo LICENSE na raiz deste repositório.

Autor: Rodrigo B. de Sousa Martins - @rodsmar
