# Librespeed - Página de teste de velocidade em PHP

<pre>Versão do docker-compose: v3.9</pre>

Arquivo docker-compose do projeto Librespeed. Licença: GNU Lesser General Public License 3 (LGPL3).

### Link do Projeto
<pre><href>https://github.com/librespeed/speedtest</href></pre>

Observações:

- A integração com MySQL não funciona;

- Container em macvlan, podendo ser alterado as necessidades do usuário (ver na seção Q&A).

Após as alterações, de forma a adaptar às necessidades do usuário, o container pode ser iniciado com o comando abaixo

<pre> docker-compose -f (CAMINHO DO DOCKER-COMPOSE.YML) up -d</pre>

Arquivo docker-compose.yml licenciado sob a General Public License Version 2 (GPLv2), disposta no arquivo LICENSE na raiz deste repositório

Autor: Rodrigo B. de Sousa Martins - @rodsmar