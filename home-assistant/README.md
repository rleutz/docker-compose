# Home Assistant - Plataforma de Automação Residencial Open Source

<pre>Versão do docker-compose: v2.3</pre>

Arquivo docker-compose do projeto Home-Assistant, licenciado sob a Apache License (ASL)

### Link do Projeto
<pre><href>https://www.home-assistant.io</href></pre>

Observações:

- A integração com alguns serviços providos pelo Home-Assistant necessitam de conexão com a internet;

- O recurso de nuvem não foi testado;

Após concluídas as modificações necessárias para o docker-compose, adaptando-o às suas necessidades e configurações da rede, é possível inicializar este container pelo comando:

<pre> docker-compose -f (CAMINHO DO DOCKER-COMPOSE.YML) up -d</pre>

Arquivo docker-compose.yml licenciado sob a General Public License Version 2 (GPLv2), disposta no arquivo LICENSE na raiz deste repositório

Autor: Rodrigo B. de Sousa Martins - @rodsmar