# docker-compose

<pre>
  Licença: GPL v2.0
</pre>

Arquivos para docker-compose.

## Redes
- frontend:
  - Rede externa com acesso a internet.
<pre>
docker network create frontend
</pre>

- backend:
  - Rede interna sem acesso a internet(Mais usada para banco de dados).
<pre>
docker network create --internal backend
</pre>

## Instruções para o uso do createcompose.sh
Edite o arquivo de ./make_folder/Config_createcompose.conf e insira seus dados como o seguinte exemplo.
<pre>
NAME=Rodrigo Leutz
USER=@rleutz
COMPOSEVERSION=3.3
</pre>

Para utilizar utilize o seguinte comando:
<pre>
./make_folder/createcompose.sh novo-compose
</pre>

## Solução de Problemas:

Eventuais problemas conhecidos podem ser solucionados. A pasta "Q&A" contém dicas para tanto (atualizado constantemente).

## Contato:
- Canal no Telegram:
  - <a href="https://t.me/portainerstacks">https://t.me/portainerstacks</a>

## Mantenedores:

- Frédney Ramllo Moronari - @moronari
- Rodrigo B. de Sousa Martins - @rodsmar
- Rodrigo Leutz - @rleutz
