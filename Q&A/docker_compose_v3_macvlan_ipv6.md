# Comunicação do host para os containers em macvlan

Para a comunicação do host com os containers, utilizando o driver macvlan:

Para tanto, utilizem o seguinte comando, fazendo as alterações conforme a rede do usuário
#
<pre>
ip link add mac0 link eth0.50 type macvlan mode bridge

ip addr add $IP_PARA_A_REDE/32 dev mac0

ip link set mac0 up

ip route add $CIDR_REDE_IPV4 dev mac0
</pre>
#
Para a comunicação da rede IPv6:
#
<pre>
ip -6 addr add $IPV6_PARA_A_REDE/128 dev mac0
ip route add $CIDR_REDE_IPV6 dev mac0
</pre>

Autor: Rodrigo B. de Sousa Martins - @rodsmar