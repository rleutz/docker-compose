# nextcloud

Docker do servidor nextcloud.

- Autor:
  - Rodrigo Leutz - @rleutz

## Criação da tabela no servidor mysql:
<pre>
create database nextcloud;

create user 'nextcloud'@'%' identified by 'senhanextcloud';

grant all privileges on nextcloud.* to 'nextcloud'@'%';

flush privileges;
</pre>

## Acesso ao servidor:
<pre>
http://IP-HOST:8080
</pre>
