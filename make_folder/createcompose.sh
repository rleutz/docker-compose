#!/bin/bash

# Entra no diretório make_folder pra poder usar ../ na criação
cd make_folder

# Testa se variável existe
if [ -z "$1" ]
then
	echo "Precisa do nome da regra, ex: ./createcompose.sh regra-um"
	exit
fi

if [ -d "../$1" ]
then
	echo "Diretório $1 já existe."
	exit
else
# Cria diretório
	mkdir ../$1

# Pega informação do user no createcompose.conf
	NAME=`cat Config_createcompose.conf | grep NAME | awk -F"=" '{ print $2 }'`
	USER=`cat Config_createcompose.conf | grep USER | awk -F"=" '{ print $2 }'`
	COMPOSEVERSION=`cat Config_createcompose.conf | grep COMPOSEVERSION | awk -F"=" '{ print $2 }'`

# Criando README.md
	echo "# $1" > ../$1/README.md
	echo "#" >> ../$1/README.md
	echo "" >> ../$1/README.md
	echo "## Sobre:" >> ../$1/README.md
	echo "" >> ../$1/README.md
	echo "## Observações:" >> ../$1/README.md
	echo "" >> ../$1/README.md
	echo "## Comandos:" >> ../$1/README.md
	echo "<pre>" >> ../$1/README.md
	echo "" >> ../$1/README.md
	echo "</pre>" >> ../$1/README.md
	echo "" >> ../$1/README.md
	echo "#" >> ../$1/README.md
	echo "" >> ../$1/README.md
	echo "- Autor:" >> ../$1/README.md
	echo "  - $NAME - $USER" >> ../$1/README.md


# Criando docker-compose.yml
	echo "version: \"$COMPOSEVERSION\"" > ../$1/docker-compose.yml
	echo "services:" >> ../$1/docker-compose.yml
	echo "  $1:" >> ../$1/docker-compose.yml
fi
