
# Nginx com letsencrypt

Docker do nginx com um Dockerfile para build da imagem instalando letsencrypt.

- Redes:
  - frontend

<pre>
Reload nas configurações sem precisar reiniciar o nginx:

$ docker exec -it nginx-letsencrypt nginx -s reload
</pre>

#

- Autor:
  - Rodrigo Leutz - @rleutz
