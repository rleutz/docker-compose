# Gitea - Pacote de Hospedagem de Controle de Versão de Desenvolvimento (git)

## Sobre:

Projeto Gitea - Solução de hospedagem de controle de desenvolvimento, alternativa mais leve ao github/gitlab, licenciado sob a MIT License/X11 License.
#

## Observações:

- Para a utilização deste container, um servidor de banco de dados MySQL/MariaDB é requisito mandatório;

- É necessária a criação de um banco de dados MySQL/MariaDB para a utilização deste container. O cumprimento deste requisito pode ser via comando abaixo sugerido:

<pre>mysql -uroot -p
(digite a senha do usuário root de seu servidor MySQL/MariaDB)
CREATE DATABASE gitea CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
grant all privileges on gitea.* to gitea@IP_SERVIDOR_GITEA identified by 'SENHA-USUARIO-DB-GITEA';
quit;</pre>

#

## Comandos:

Após a configuração do banco de dados a inicialização do container pode ser feita via:
<pre>(docker-compose -f caminho-docker-compose.yml up -d)</pre> 

O acesso pode ser feito via web (http://IP_GITEA:3000), onde o usuário acessará a página de configuração do servidor Gitea. 

Mais informações acerca da configuração do seu servidor Gitea, acesse o site do projeto:
<href>https://docs.gitea.io/</href>

#

- Autor:
  - Rodrigo B. de Sousa Martins - @rodsmar

#
* Este arquivo docker-compose.yml está licenciado sob a General Public License Version 2 (GPLv2), disponível na raiz deste repositório.