# Portainer-ce

Docker-compose para portainer com suporte a ssl.

- Templates:
  - <a href="https://github.com/Qballjos/portainer_templates">https://github.com/Qballjos/portainer_templates</a>

<pre>
Gerar chaves:

$ openssl genrsa -out portainer.key 2048

$ openssl ecparam -genkey -name secp384r1 -out portainer.key

$ openssl req -new -x509 -sha256 -key portainer.key -out portainer.crt -days 3650
</pre>
