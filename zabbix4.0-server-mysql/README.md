# Zabbix 4.0 LTS (Servidor e Frontend)
<pre>Base: Alpine Linux 
Versão do docker-compose: v2.3 
</pre>

### Observações

- Para a utilização deste container, é necessário um db mysql já criado para abrigar as tabelas do zabbix (tanto o servidor quanto o frontend);

- Em razão de este container funcionar no MySQL/MariaDB, a utilização em cartão SD não é recomendada, ante o risco de reduzir drasticamente a vida útil da unidade de armazenamento (caso o usuário utilize uma Raspberry Pi ou semelhantes);

- Por alguma razão, o charset padrão que o zabbix utiliza em seu banco de dados não escreve as tabelas necessárias corretamente, o que ocasiona erros no momento de sua instalação. Assim, a recomendação é de o próprio usuário crie o banco de dados, utilizando charset compatível (ex: utf8mb4);

<pre>Conforme a informação acima disposta, o usuário pode criar o banco de dados utilizando os comandos abaixos como exemplo:

mysql -uroot -p
(digite a senha root do seu servidor mysql)
CREATE DATABASE zabbix CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
grant all privileges on zabbix.* to zabbix@localhost identified by '<password>';
quit;
</pre>

- Em razão de este container funcionar no MySQL/MariaDB, a utilização em cartão SD não é recomendada, ante o risco de reduzir drasticamente a vida útil da unidade de armazenamento (caso o usuário utilize uma Raspberry Pi ou semelhantes);

- O Zabbix Agent apontado para o endereço de localhost não funciona por padrão, recomenda-se que o usuário instale o Zabbix-Agent na máquina host, apontando no arquivo de configuração do agente o endereço IP do servidor Zabbix:

<pre> server=IP_DO_CONTAINER_SERVIDOR_ZABBIX </pre>

Após concluídas as modificações necessárias no docker-compose.yml, adaptando-o a sua rede (e/ou as suas necessidades) é possível iniciar utilizando o comando

docker-compose -f (CAMINHO DO DOCKER-COMPOSE.YML) up -d
